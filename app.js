var app = new Vue({
    el:'#app',

    data: {
        message: "Hai Vue !"
    }
});

var app2 = new Vue({
    el:'#app-2',
    data: {
        message2: 'You loaded this page on' + new Date().toLocaleString()
    }

});

var app3 = new Vue({
    el:'#app-3',
    data:{
        see: true
    }
});

var app4 = new Vue({
    el:'#app-4',
    data:{
        todos:[
            { text: 'Lear JS' },
            { text: 'Learn Py' },
            { text: 'Learn something different' }
        ]
    }
});

app4.todos.push({ text: 'Im New' })

var app5 = new Vue({
    el: '#app-5',
    data:{
        message5: 'Hai, i just greating you.'
    },

    methods: {
        reverseMessage: function(){
            this.message5 = this.message5.split('').reverse().join('')
        }
    }
});

var app6 = new Vue({
    el: '#app-6',
    data: {
        message6: 'Say hai to me.!'
    }
});

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
});

var app7 = new Vue({
    el: '#app-7',
    data: {
        blacklist: [
            { id: 0, text: 'Vegetables' },
            { id: 1, text: 'Banana' },
            { id: 2, text: 'What ever i want.!' }
        ]
    }
})
